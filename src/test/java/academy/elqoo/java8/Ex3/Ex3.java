package academy.elqoo.java8.Ex3;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Ex3 {

    @Test
    public void ex3_1() {
        String[] s1 = {"This", "That"};
        Integer[] i1 = {1, 2, 3};

        Assert.assertEquals("This", Ex3_1.printArray(s1, o -> o.equals("This")).get(0));
        Assert.assertEquals(2, Ex3_1.printArray(i1, o -> o.equals(2)).get(0));
    }

    @Test
    public void ex3_2() {
        AnimalHouse<Cat> house = new AnimalHouse<Cat>();
        AnimalHouse<Dog> house2 = new AnimalHouse<Dog>();
        AnimalHouse<Cat> house3 = new AnimalHouse<Cat>();
        house3.setAnimal(new Cat());
        AnimalHouse<Dog> house4 = new AnimalHouse<Dog>();
        house4.setAnimal(new Dog());
    }


    @Test
    public void ex3_4() {
        String[] s1 = {"This", "That"};
        Ex3_1.exchangePlaces(s1, 0, 1);

        Assert.assertEquals("This", s1[1]);
        Assert.assertEquals("That", s1[0]);

    }

    @Test
    public void ex3_5() {
        String[] s1 = {"This", "That", "Those", "Arroz","Potatoes"};
        Assert.assertEquals("That", Ex3_1.maxInRange(s1, 0,2));
        Assert.assertEquals("Arroz", Ex3_1.maxInRange(s1, 0,s1.length));
    }

    @Test
    public void ex3_6() {
        String[] s1 = {"This", "That"};
        Integer[] i1 = {1, 2, 3};

        Assert.assertEquals(1, Ex3_1.countIf(s1, o -> o.equals("This")));
        Assert.assertEquals(2, Ex3_1.countIf(i1, o -> o < 3));
    }

    @Test
    public void ex3_7() {
        Library<Book> bookLibrary = new Library<>();
        Library<Video> videoLibrary = new Library<>();
        Library<Newspaper> newspaperLibrary = new Library<>();

        Book book = new Book();
        book.setName("New Book");
        bookLibrary.addMedia(book);
        Video video = new Video();
        video.setName("New Video");
        videoLibrary.addMedia(video);
        Newspaper newspaper = new Newspaper();
        newspaper.setName("New Newspaper");
        newspaperLibrary.addMedia(newspaper);

        Assert.assertEquals("New Book",bookLibrary.getLast().name);
        Assert.assertEquals("New Video",videoLibrary.getLast().name);
        Assert.assertEquals("New Newspaper",newspaperLibrary.getLast().name);

        BookLibrary bookLibrarys = new BookLibrary();
        VideoLibrary videoLibrarys = new VideoLibrary();
        NewspaperLibrary newspaperLibrarys = new NewspaperLibrary();

        bookLibrarys.addMedia(new Book());
        videoLibrarys.addMedia(new Video());
        newspaperLibrarys.addMedia(new Newspaper());

        Assert.assertEquals("Book",bookLibrarys.getLast().name);
        Assert.assertEquals("Video",videoLibrarys.getLast().name);
        Assert.assertEquals("Newspaper", newspaperLibrarys.getLast().name);

    }

}
