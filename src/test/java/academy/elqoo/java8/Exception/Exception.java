package academy.elqoo.java8.Exception;

import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.fail;

public class Exception {

    /*@Test
    public void test1(){
        Assert.assertEquals(1, MyException.main(" "));
    }*/

    @Test(expected = MyException.class)
    public void test2() throws  MyException{
        throw new MyException("Something Happened");
    }

    @Test
    public void test3() throws  MyException{
        try {
            Except.throwMyException();
        }catch (MyException my){
            my.printStackTrace();
        }
    }

    @Test(expected = NullPointerException.class)
    public void test4_1(){
        String i = null;
        i.isBlank();
    }

    @Test
    public void test4_2(){
        String i = null;
        try {
            i.isBlank();
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }

    }

    @Test
    public void test5(){
        String i = null;
        try {
            Except.f();
        }catch (MyException me){
            me.printStackTrace();
        }

    }

    @Test(expected = RuntimeException.class)
    public void test6(){
        String i = null;
        Except.fRT();
    }

    @Test
    public void test7(){
        try {
            Except.throws3();
        }catch (MyException | MyException_v2 | ArrayIndexOutOfBoundsException ex ){
            ex.printStackTrace();
        }
    }

    @Test
    public void test8(){
        int counter = 0;
        boolean throwing = true;
        while (throwing){
            throwing = false;
            try {
                if (counter > 5)
                    Except.hehe("");
                else
                    Except.hehe("Nope");
            }catch (IllegalArgumentException e){
                e.printStackTrace();
                throwing = true;
            }finally {
                counter++;
            }

        }
        Assert.assertEquals(7,counter);
    }


    @Test
    public void test9(){

        try {
            Except.throwMyException();
            fail();
        }catch (MyException e1){
            Assert.assertEquals("Asked for v1", e1.getMessage());
            try {
                Except_v2.throwMyException();
                fail();
            }catch (MyException e2){
                Assert.assertEquals("Asked for v2", e2.getMessage());
                try {
                    Except_v3.throwMyException();
                    fail();
                }catch (MyException e3){
                    Assert.assertEquals("Asked for v3", e3.getMessage());
                    try {
                        ((Except) new Except_v3()).throwMyException();
                        fail();
                    }catch (MyException e4){
                        Assert.assertEquals("Asked for v1", e4.getMessage());
                    }
                }
            }
        }
    }

    @Test(expected = FileNotFoundException.class)
    public void test10() throws IOException {
        String[] s = {"e","e"};
        ResourceManagement.main(s);
    }

}
