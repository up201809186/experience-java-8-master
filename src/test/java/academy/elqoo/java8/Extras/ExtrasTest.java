package academy.elqoo.java8.Extras;

import academy.elqoo.java8.extras.Employee;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class ExtrasTest {
    @Test
    public void whenIncrementSalaryForEachEmployeeThenApplyNewSalary() {

        // Implement salaryIncrement method

        // Use forEach to call salaryIncrement and increase the salary by 10.0
        Employee.employees.forEach(employee -> employee.salaryIncrement(1.1));
        assertThat(Employee.employees, Matchers.contains(
                Matchers.hasProperty("salary", Matchers.equalTo(110000.0)),
                Matchers.hasProperty("salary", Matchers.equalTo(220000.0)),
                Matchers.hasProperty("salary", Matchers.equalTo(330000.0))
        ));
    }

    @Test
    public void whenMapIdToEmployeesThenGetEmployeeStream() {

        Integer[] empIds = { 1, 2, 3 };

        //Given the list of ids, use the method map to return a list of employees
        // Implement a method findById in Employee class to return a employee by id

        List<Employee> employees = Employee.findByIDs(empIds);
        assertEquals(Employee.employees.size(), empIds.length);
    }

    @Test
    public void whenFindFirstThenGetFirstEmployeeInStream() {
        Integer[] empIds = { 1, 2, 3, 4 };

        // Map the employees using findById method and then
        // return the first employee with the salary greater than 100000. If no such employee exists, then null is returned
        Employee employee = Employee.findByIDs(empIds).stream().filter(employee1 -> employee1.getSalary() > 100000).findFirst().get();
        assertEquals(employee.getSalary(), 200000, 0);
    }

    @Test
    public void whenSortStreamThenGetSortedStream() {

        //Return an ordered list using the method sorted

        List<Employee> employees = Employee.getEmployees();
        employees.sort(Comparator.comparing(Employee::getName));
        assertEquals(employees.get(0).getName(), "Bill Gates");
        assertEquals(employees.get(1).getName(), "Jeff Bezos");
        assertEquals(employees.get(2).getName(), "Mark Zuckerberg");
    }

    @Test
    public void whenFindMinThenGetMinElementFromStream() {

//        Sort the list and return the employee with the smallest id using the min method
        Employee firstEmp = Employee.getEmployees().stream().min(Comparator.comparing(Employee::getId)).get();
        assertEquals(firstEmp.getId(), 1);
    }

    @Test
    public void whenFindMaxThenGetMaxElementFromStream() {

        // Return the employee with the higher salary
        // Use max method and Comparator.comparing
        Employee maxSalEmp = Employee.getEmployees().stream().max(Comparator.comparing(Employee::getId)).get();
        assertEquals(maxSalEmp.getSalary(), 300000.0,0);
    }

    @Test
    public void whenParallelStreamThenPerformOperationsInParallel() {

        // Implement salaryIncrement method
        // Use forEach to call salaryIncrement and increase the salary by 10.0
        // Use parallel syntax and read bellow to understand

        List<Employee> empList = Employee.getEmployees().stream().collect(Collectors.toList());
        empList.parallelStream().forEach(employee -> employee.salaryIncrement(1.1));
        assertThat(empList, Matchers.contains(
                Matchers.hasProperty("salary", Matchers.equalTo(110000.0)),
                Matchers.hasProperty("salary", Matchers.equalTo(220000.0)),
                Matchers.hasProperty("salary", Matchers.equalTo(330000.0))
        ));
    }

//
//    Here salaryIncrement() would get executed in parallel on multiple elements of the stream, by simply adding the parallel() syntax.
//
//    This functionality can, of course, be tuned and configured further, if you need more control over the performance characteristics of the operation.
//
//    As is the case with writing multi-threaded code, we need to be aware of few things while using parallel streams:
//
//    We need to ensure that the code is thread-safe. Special care needs to be taken if the operations performed in parallel modifies shared data.
//    We should not use parallel streams if the order in which operations are performed or the order returned in the output stream matters. For example operations like findFirst() may generate the different result in case of parallel streams.
//            Also, we should ensure that it is worth making the code execute in parallel. Understanding the performance characteristics of the operation in particular, but also of the system as a whole — is naturally very important here.

}
