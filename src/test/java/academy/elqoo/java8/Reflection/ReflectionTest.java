package academy.elqoo.java8.Reflection;


import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Collectors;


public class ReflectionTest {

    @Test
    public void test1() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {

        Assert.assertEquals("academy.elqoo.java8.Reflection.Reflections", Reflections.class.getName());
        Assert.assertEquals("java.lang.Object", Reflections.class.getSuperclass().getName());
        Assert.assertEquals("java.io.Serializable", Arrays.stream(Reflections.class.getInterfaces()).map(Class::getName).collect(Collectors.joining()));
        Assert.assertEquals("Java", Reflections.class.getConstructor().newInstance().getName());

    }

}
