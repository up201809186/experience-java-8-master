package academy.elqoo.java8.Generics;

import academy.elqoo.java8.Ex3.Book;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;

public class Tester {

    @Test
    public void ex1(){
        Jar<Book> jarOfBooks = new Jar<>();

        jarOfBooks.add(new Book());
        jarOfBooks.add(new Book());

        Book book = jarOfBooks.remove();

    }

    @Test
    public void ex2(){
        String[] s1 = ArrayGenerator.create(10,"Potatoes");
        Assert.assertEquals("Potatoes", Arrays.asList(s1).get(0));
        Assert.assertEquals(10,s1.length);

        Integer[] i1 = ArrayGenerator.createWithClass(Integer.class,10,100);
        Assert.assertEquals(100, Arrays.asList(i1).get(0).intValue());
        Assert.assertEquals(10,i1.length);
    }

    @Test
    public void ex3(){
        Assert.assertNull(Scale.heavier(0,0));
        Assert.assertEquals("c", Scale.heavier("a", "c"));
        Assert.assertEquals(2, Scale.heavier(1,2 ).intValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void ex4() throws  IllegalArgumentException{

        List<Integer> intList = new ArrayList<>();
        Collections.addAll(intList, 1,2,3,4,5,6,7,8,9,20);

        List<String> stringList = new ArrayList<>();
        Collections.addAll(stringList, "Someone","Potato","Zebra");

        List<Integer> objectList = new ArrayList<>();

        Assert.assertEquals(20, ListUtils.getMax(intList).intValue());
        Assert.assertEquals("Zebra", ListUtils.getMax(stringList));
        Assert.assertNotNull(ListUtils.getMax(objectList));
    }

    @Test
    public void ex5(){
        List<String> stringList = new ArrayList<>();
        Collections.addAll(stringList, "Someone",null,"Zebra");

        Assert.assertEquals(1, ListUtils.getNullIndices(stringList).get(0).intValue());
    }

    @Test
    public void ex6(){
        List<Integer> intList1 = new ArrayList<>();
        Collections.addAll(intList1, 1,2,3,4,5,6,7,8,9,20);
        List<Integer> intList2 = new ArrayList<>();
        Collections.addAll(intList2, 1,2,3,4,5,6,7,8,9,20);
        List<Double> intList3 = new ArrayList<>();
        Collections.addAll(intList3, 1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,20.0);

        List<List<? extends Number>> lists = new ArrayList<>();
        Collections.addAll(lists, intList1,intList2,intList3);

        Assert.assertEquals(30, ListUtils.flatten(lists).size());
    }

    @Test
    public void ex7(){
        List<Integer> intList1 = new ArrayList<>();
        Collections.addAll(intList1, 1,2,3,4,5,6,7,8,9,20);
        List<Integer> intList2 = new ArrayList<>();
        Collections.addAll(intList2, 1,2,3,4,5,6,7,8,9,20);

        Assert.assertEquals(20, ListUtils.addAll(intList1,intList2).size());
    }

}
