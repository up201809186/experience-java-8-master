package academy.elqoo.java8.Ex3;

import java.text.CollationElementIterator;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Ex3_1 {

    public static<T> List printArray(T[] t1 , Predicate<T> predicate){
        return Arrays.stream(t1).filter(predicate).collect(Collectors.toList());
    }

    public static<T> T[] exchangePlaces(T[] t, int pos1, int pos2){
        T temp1 = t[pos1];
        T temp2 = t[pos2];
        t[pos1] = temp2;
        t[pos2] = temp1;
        return t;
    }

    public static<T extends Comparable> T maxInRange(T[] t, int min, int max){
        List<T> temp=  Arrays.stream(t).collect(Collectors.toList()).subList(min,max);
        temp.sort(Comparator.naturalOrder());
        return temp.get(0);
    }

    public static<T> int countIf(T[] t1 , Predicate<T> predicate){
        return Arrays.stream(t1).filter(predicate).collect(Collectors.toList()).size();
    }

}
