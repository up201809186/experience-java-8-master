package academy.elqoo.java8.Ex3;

import academy.elqoo.java8.Ex3.Interfaces.Media;

import java.util.ArrayList;
import java.util.List;

public class Library<T extends Media> {

    List<T> library = new ArrayList<>();

    public void addMedia(T media){
        library.add(media);
    }

    public T getLast(){
        return library.get(library.size()-1);
    }

}
