package academy.elqoo.java8.Ex3;

import academy.elqoo.java8.Ex3.Interfaces.Media;

import java.util.ArrayList;
import java.util.List;

public class BookLibrary {

    List<Book> library = new ArrayList<>();

    public void addMedia(Book media){
        library.add(media);
    }

    public Book getLast(){
        return library.get(library.size()-1);
    }

}
