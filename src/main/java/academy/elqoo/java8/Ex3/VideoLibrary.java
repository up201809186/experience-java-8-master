package academy.elqoo.java8.Ex3;

import java.util.ArrayList;
import java.util.List;

public class VideoLibrary {

    List<Video> library = new ArrayList<>();

    public void addMedia(Video media){
        library.add(media);
    }

    public Video getLast(){
        return library.get(library.size()-1);
    }

}
