package academy.elqoo.java8.Ex3;

import java.util.ArrayList;
import java.util.List;

public class NewspaperLibrary {

    List<Newspaper> library = new ArrayList<>();

    public void addMedia(Newspaper media){
        library.add(media);
    }

    public Newspaper getLast(){
        return library.get(library.size()-1);
    }

}
