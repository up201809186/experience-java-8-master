package academy.elqoo.java8.Exception;

public class MyException extends Exception {
/*
    public static int main(String args) {

        try{
            if(args.isBlank())
                throw new Exception("Potato");
        }catch (Exception e){
            e.printStackTrace();
            return 1;
        }

        return 0;
    }*/

    public MyException(String message) {
        super(message);
    }
}
