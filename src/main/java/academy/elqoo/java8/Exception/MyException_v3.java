package academy.elqoo.java8.Exception;

public class MyException_v3 extends ArrayIndexOutOfBoundsException {

    public MyException_v3(String message) {
        super(message);
    }
}
