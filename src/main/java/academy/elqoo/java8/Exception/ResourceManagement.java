package academy.elqoo.java8.Exception;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ResourceManagement {
    public static void main(String[] args) throws IOException {

        try (BufferedReader br = new BufferedReader(new FileReader("C://test.txt"))){
            System.out.println(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }
}
