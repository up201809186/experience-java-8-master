package academy.elqoo.java8.Exception;

public class MyException_v2 extends Exception {

    public MyException_v2(String message) {
        super(message);
    }
}
