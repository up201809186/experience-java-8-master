package academy.elqoo.java8.Exception;

public class Except {

    public static void throwMyException()throws MyException{

        throw new MyException("Asked for v1");

    }

    public static void f() throws MyException{
        try{
            g();
        }catch (MyException_v2 myException_v2){
            myException_v2.printStackTrace();
            throwMyException();
        }
    }

    public static void g() throws MyException_v2{
        throw new MyException_v2("Potato");
    }

    public static void fRT(){
        try{
            g();
        }catch (MyException_v2 myException_v2){
            throw new RuntimeException("", myException_v2);
        }
    }

    public static void throws3() throws MyException,MyException_v2,MyException_v3{
        throw new MyException_v3("");
    }

    public static void hehe(String i){
        if(!i.isBlank())
            throw new IllegalArgumentException("HEHE");
    }
}
