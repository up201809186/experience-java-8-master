package academy.elqoo.java8.defaultmethods;

import java.util.List;

public interface Shape {

    int getXPos();

    int getYPos();

    void setXPos(int xPOs);

    void setYPos(int yPos);

    default String getName(){
        return "";
    }

    default void move(int x, int y){
        setXPos(getYPos() + x);
        setYPos(getYPos() + y);
    }

    static void moveXPosWith10(List<AbstractShape> shape){
        shape.forEach(abstractShape ->  abstractShape.setXPos(abstractShape.getYPos() + 10));
       ;
    }

    default void notImplementedMethod() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("");
    };
}
