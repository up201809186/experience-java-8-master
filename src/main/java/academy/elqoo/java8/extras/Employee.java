package academy.elqoo.java8.extras;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Employee {

    public static List<Employee> employees = new ArrayList(){{
        add(new Employee(1, "Jeff Bezos", 100000.0));
        add(new Employee(2, "Bill Gates", 200000.0));
        add(new Employee(3, "Mark Zuckerberg", 300000.0));
    }};

    private int id;
    private String name;
    private double salary;

    public Employee(int id, String name, double salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public static List<Employee> getEmployees() {
        return new ArrayList(){{
            add(new Employee(1, "Jeff Bezos", 100000.0));
            add(new Employee(2, "Bill Gates", 200000.0));
            add(new Employee(3, "Mark Zuckerberg", 300000.0));
        }};
    }

    public static void setEmployees(List<Employee> employees) {
        Employee.employees = employees;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Employee findByID(int id){
        return employees.stream().filter(employee -> employee.getId()==id).findAny().get();
    }

    public static List<Employee> findByIDs(Integer[] ids){
        return Employee.getEmployees().stream().filter(employee -> Arrays.asList(ids).contains(employee.getId())).collect(Collectors.toList());
    }

    public void salaryIncrement(double amount) {
        this.salary = (this.salary * amount);
        DecimalFormat df = new DecimalFormat("###.##");
        this.salary = Double.parseDouble(df.format(this.salary));
    }
}
