package academy.elqoo.java8.Generics;

import java.util.ArrayList;
import java.util.List;

public class Jar<T> {
        private List<T> list = new ArrayList<>();

        public void add(T item){
            list.add(item);
        }

        public T remove(){
            return list.remove(list.size()-1);
        }
}
