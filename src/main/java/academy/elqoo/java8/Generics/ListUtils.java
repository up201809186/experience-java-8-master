package academy.elqoo.java8.Generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ListUtils {

    public static<T extends Comparable> T getMax(List<T> list){
        if(list.isEmpty())
            throw new IllegalArgumentException();

        return (T) list.stream().max(Comparator.naturalOrder()).get();
    }

    public static<T extends Comparable> T getMin(List<T> list){
        if(list.isEmpty())
            throw new IllegalArgumentException();

        return (T)  list.stream().min(Comparator.naturalOrder()).get();
    }

    public static<T> List<Integer> getNullIndices(List<T> list){
        List<Integer> temp = new ArrayList<>();
        for(int i = 0;  i < list.size(); i++)
            if(list.get(i) == null)
                temp.add(i);
        return temp;
    }

    public static<T> List<T> flatten(List<List<? extends T>> lists){
        List<T> flat = new ArrayList<>();
        for(List<? extends T> list: lists)
            flat.addAll(list);
        return flat;
    }
    public static<T> List<T> addAll(List<? extends T> list1, List<? extends T> list2){
        List<T> temp = new ArrayList<>();
        temp.addAll(list1);
        temp.addAll(list2);
        return temp;
    }

}
