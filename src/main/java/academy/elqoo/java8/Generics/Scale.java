package academy.elqoo.java8.Generics;

public class Scale {

    public static<T> T heavier(T t1, T t2){
        int result = t1.toString().compareTo(t2.toString());
        if (result == 0)
                return null;
        return  result > 0 ? t1 :t2 ;
    }

}
