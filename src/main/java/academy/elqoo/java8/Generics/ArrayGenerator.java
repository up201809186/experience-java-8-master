package academy.elqoo.java8.Generics;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.stream.Stream;

import static java.util.Arrays.*;

public final class ArrayGenerator {

    static<T> T[] create(int length, T defaultItem){
        return fill((T[])Array.newInstance(defaultItem.getClass(), length),defaultItem);
    }

    static<T> T[] createWithClass(Class<T> classType, int length, T defaultItem){
        return fill((T[])Array.newInstance(classType, length),defaultItem);
    }


    private static <T> T[] fill(T[] newInstance, T defaultItem) {
        for(int i = newInstance.length -1; i >= 0; i--){
            newInstance[i] = defaultItem;
        }
        return newInstance;
    }

}
